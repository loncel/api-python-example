#**
#* @file example.py
#* @description Example code to demonstrate the usage of the Loncel API
#*
#* @author Dirk Grobler
#* @copyright Copyright 2021 - Lonceltech Ltd. All Rights Reserved.
#*/

import json
import requests
from requests_aws4auth import AWS4Auth

config = {'API_KEY': 'your api key granted by Loncel',
          'USER': 'user login to access your sites',
          'PWD': 'user pwd',
          'REGION': 'ap-southeast-2'
}

headers = {
    'host': "api.frostsmart.com",
    'x-api-key':config['API_KEY']
}

service = 'execute-api'

# access the session with config 
def getSession():
    data = '''{"user":"%s", "password": "%s", "region": "%s", "includeCredentials": true}''' % (config['USER'], config['PWD'], config['REGION'])
    response = requests.post('https://api.loncel.com/v1/user/login', 
        data = data , headers = headers)
    return response.json()

# 1. Get the session 
session = getSession()

# 2. Auth to sign requests
auth = AWS4Auth(session['credentials']['accessKeyId'], session['credentials']['secretAccessKey'], config['REGION'], service,
                    session_token=session['credentials']['sessionToken'])


print('\nLIST METRICS++++++++++++++++++++++++++++++++++++\n')
response = requests.get('https://api.frostsmart.com/v1/metrics/list', headers=headers, auth=auth)    
print(response.json())

print('\nLIST UNITS++++++++++++++++++++++++++++++++++++\n')
response = requests.get('https://api.frostsmart.com/v1/units', headers=headers, auth=auth)    
print(response.json())

print('\nLIST SENSORS ATTACHED TO A PARTICULAR UNIT++++++++++++++++++++++++++++++++++++\n')
response = requests.get('https://api.frostsmart.com/v1/sensors?unit=474', headers=headers, auth=auth)    
print(response.json())

print('\nFETCH WATER VOLUME DATA++++++++++++++++++++++++++++++++++++\n')
# query the avg for water volume metric for unit 474 broken down by sensor (water meter)
query = '''{
            "startRelative": {
                "value": "1",
                "unit": "weeks"
            },
            "metrics": [{
                "name": "loncel.water.volume",                
                "groupBy": ["sensor"],
                "filter": {
                    "unit": "474"
                },
                "aggregator": {
                    "name": "avg",
                    "sampling": {
                        "value": 1,
                        "unit": "days"
                    }
                }
            }]
        }
'''
response = requests.post('https://api.frostsmart.com/v1/metrics/query', data=query, headers=headers, auth=auth)
print(response.json())